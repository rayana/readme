# Rayana's README

This README intends to help anyone that works directly with me at GitLab to know a little bit more about me, how I think, and how I work.

* [🦊 My GitLab handle `@rayana`](https://gitlab.com/rayana/)
* [🏀 Team page](https://about.gitlab.com/company/team/#rayana)

## Who I am

*  My name is Rayana Verissimo and I'm the [Product Design Manager](https://handbook.gitlab.com/job-families/product/product-design-management/) for Ops and SaaS Platforms at GitLab. Prior to this, I was the Staff Product Designer for the Testing and Runner teams, and Senior Product Designer for the (now defunct) Release Management team. I am originally from [Recife, Brazil 🇧🇷](https://en.wikipedia.org/wiki/Recife) and I currently live in The Netherlands 🇳🇱. My beloved cat [Panqueca](https://about.gitlab.com/company/team-pets/#40-panqueca) passed in June 2023.
* I have a BTech in Graphic Design and I have been working with the web for over 10 years. I am an all-rounded designer that can work from validation of a business problem to the hands-on implementation of an interface. I enjoy building things. For a full background, you can check out my [LinkedIn profile](https://www.linkedin.com/in/rayanaverissimo/).
* I am more of a realist than a visionary.
* I believe in [collaboration](https://about.gitlab.com/handbook/values/#collaboration) and [transparency](https://about.gitlab.com/handbook/values/#transparency) above all.

<details>
  <summary markdown="span">👀 Psst... Want to know more?</summary>

     Here's some trivia about me:

     * I have always been a designer. Altought I am extremely fulfilled, I'd still like to experiment with a different career in the future.
     * Prior to GitLab, I was an UI Engineer building Apple's very first web application style guide. That experience shaped me into the person and professional I am today. 
     * I can think about things from an engineer's mindset.
     * All those years working as a visual designer gave me a _pixel-perfect_ eyesight. It's both a blessing and a curse.
     * As designer, I am responsible for the product as a whole. You will see me involved in many, many different fronts: from process to hands-on coding.
     * I prototype low-fidelity first. You will not see me delivering pixel-perfect visuals until they are required in the development process. I prefer to gather ideas and work on them in words before delivering any "designs".

</details>

### Personality tests

* [Social Style](https://about.gitlab.com/handbook/leadership/emotional-intelligence/social-styles/) - [Expressive](https://gitlab.com/rayana/readme/-/blob/master/social-style-expressive.png): intuitive, right down the middle.
* [INTJ-T](https://www.16personalities.com) - [Architect](https://www.16personalities.com/intj-personality): independent to the core, want to shake off other people’s expectations and pursue their own ideas.
* [Enneagram](https://www.enneagraminstitute.com/type-descriptions) - [Individualist](https://www.enneagraminstitute.com/type-4): sensitive, introspective, expressive, dramatic, self-absorbed, and temperamental.
* [Design Type](https://www.whatkindofdesigner.com/) - [Number Cruncher](https://www.whatkindofdesigner.com/design-type/number-cruncher): can imagine scenarios that others can’t yet picture, taking the first steps towards describing a better way to do something.

### Personal principles

1. Allow events to change you and your craft.
1. Design for outcome.
1. Tell the real story.
1. Failure is inevitable.
1. Respect your own limitations.
1. Remember to have fun.

## Availability

* **How to reach me:**
     * **Slack**: DM or mention is the best way to reach me during the day. 
     * **Email**: I read my emails multiple times a day but I will default to my [weekly plan](https://gitlab.com/rayana/plan/) for priorities. You should not feel obligated to read or respond to any messages you receive from me outside of your normal hours.
     * **GitLab**: Please @ me directly in comments in GitLab so I see your message, and assign MRs to me. 
     * **Phone**: I rather not to be contacted on text message/WhatsApp or phone call.
* My timezone is [CET](https://time.is/CET). 
     * I generally work from ~10:00 to ~18:30. I am not a morning person and I'm useless before 10 am. I usually do not accept meetings late-nights or early mornings. If you see a good time on my calendar inside my working hours, please schedule something (no need to ask first). You can do so either via Google Calendar (preferred) or using [my Calendly page](https://calendly.com/rayana). 
     * You will see my calendar blocked with a `Busy` event whenever I take personal time during the day. 
     * If you need to reschedule something, go ahead! All my calendar events are set with modify privileges and you should feel free to go ahead and move it to some other time slot that is within business hours and not conflicting with other meetings.
     * Friday is my meeting-free day. If my schedule is open, DM me on Slack and I will make sure we talk that day.

## Work philosophy

* At GitLab, I see myself as an enabler of change. My growth is directly linked to the success of people around me. My role is to foster engagement and empowerment.
* I take pride in the relationships I build at work. I aim to have an authentic, open, trusting relationship with you. I will not lie to you about how I feel, or what is going on inside my head - that can make me come off as negative sometimes. I will work towards building a sense of companionship, so please reach out if you want to chat!
* I assume you are very good at your job, and that you are here to deliver great value to our company and people. You are the expert as well. I will work to provide necessary context and ask questions to help you vet your ideas but I will not override you.
* You will let me know if I am blocking you. One of my main responsibilities is ensuring that we are set up for success. Please reach out and let me know how I can help.
* I have a strong sense of ownership when it comes to work. Related to that, I do not like inefficiency and will look for any way to improve that.
* I am strong with program planning and documentation. I **love** taking notes during meetings. Please help me with these! ✍️
* I am impacient and I do not ask for permission. I turn ideas into actionable items and I will make sure you are involved.
* I prioritize my [weekly tasks](https://gitlab.com/rayana/plan/) in the open.

## Communication style

* My preferred method of communication is verbal. I am quite talkative and expressive (_jazz hands_) and you will see that translated into my writting across GitLab. I also like learning through narrative, so sometimes I will send you an invite to a Zoom call if I need you to explain things to me.
* English is not my native language which can affect how I verbally express myself. I experience a lot of non-linear thinking in a mixture of Portuguese and English, which reflects in my choice of words or sentence structure. In other words, I think as I speak.
* I ask questions. I am not afraid of not knowing things, and I will seek for help whenever necessary. If it feels like I am questioning you it is because I am either: a) trying to gather context or, b) trying to understand if everyone is on the same page.

## 1:1s

* I value connecting with people regularly and I hold weekly 1:1s with all my direct reports. I enjoy to experience the almost "in-person" interactions of a Zoom call.
* By default, my direct reports should schedule a weekly, 30 minute conversation with me. I prefer that you schedule the meeting, as you can pick a time that works for your time zone.
* Here is my [1:1 meeting template](https://docs.google.com/document/d/1pteTuKpUlYyjLaHDk4lVb-4SpmxKC03U8eSyxohSgBE/edit?usp=sharing) (internal only). I believe 1:1s are for you to set the agenda. What would you like to talk about? What is going well? What is bugging you? How can I help you? If there are things that I want to ask you, I will do it, but this is your time. If I have feedback, I will also provide it during this time. We will track all discussions in a private Google Doc. You will have full access and control over it.

## Feedback

* I love feedback — it is critical to my success here. I prefer to give and receive written feedback (async), so the information can be first digested, to be then discussed together (sync). You will find me asking for ongoing feedback on things I can do better, things I can improve and things I am doing well. I want feedback to be a two-way street. If you are ever surprised by any feedback from me on your reviews then I have not done my job right.
* I enjoy receiving public praise, but private praise fuels me.
* Although performance feedback is typically [not public by default](https://about.gitlab.com/handbook/communication/#not-public), in the spirit of [transparency](https://about.gitlab.com/handbook/values/#transparency), I am choosing to share my most recent 360 feedback here:
     * [June 2020](https://drive.google.com/file/d/1M8-dIJT0DqlsNhVoGKrHjctOWLV-htak/view?usp=sharing)
     * [January 2019](https://drive.google.com/file/d/182j-FnV2lj1kfFMgl6cN56RQPEnxPLFY/view?usp=sharing)
     * I may or may not share future 360 reviews as I always want others to be able to provide me with candid feedback, especially about areas for improvement, without fear that their words (even anonymously) may be made public.
* 👉 You can also provide me direct (anonymous) feedback by filling out my ongoing [feedback form](https://forms.gle/HRLA6CGjpDxnZsgx8).
* I value stability and predictablity - I hate feeling uncomfortable. Sometimes I can react negatively to change but I do my best to embrace it and keep a positive mindset. I recognize that growing means feeling uncomfortable.

## Footnotes

This is not meant to be a static file, and its contents will change as I learn how to navigate through life at GitLab. I would love your feedback! Did you find the time you spent reading this valuable? Was something critical missing? Feel free to go ahead and submit an MR to this document. If you see me not living up to anything included please let me know 😉. It is possible that I have changed my mind, or that I am just dropping the ball. Either way, get in touch!
